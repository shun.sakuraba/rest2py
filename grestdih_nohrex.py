#!/usr/bin/python
# Copyright 2018 Shun Sakuraba
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



# This script runs on python 2.7. Also tested on python3 but not sure whether it runs perfectly.
# Why not python 3 on 2018? Because 3 may not be installed on some supercomputer centers. Heck.

# I made this script because
# (1) sed-based REST2 provided by PLUMED2-hrex was slow (due to O(N^2) loop at dihedrals)
# (2) Wanted a capability to also convert topologies with FEP safely
# (3) Wanted to speed the calculation up by removing charge FEP
# Kudos to partial_tempering.sh

import argparse
import sys
import math
import copy
import cmath

# I love this in C++.
def next_permutation(ls):
    if len(ls) <= 1:
        return (False, ls)
    i = len(ls) - 1
    while True:
        j = i
        i -= 1
        if ls[i] < ls[j]:
            k = len(ls) - 1
            while not (ls[i] < ls[k]):
                k -= 1
            tmp = ls[i]
            ls[i] = ls[k]
            ls[k] = tmp
            ls[j:] = ls[j:][::-1]
            return (True, ls)
        if i == 0:
            ls = ls[::-1]
            return (False, ls)

# based on gromacs 5.1.3+ behaviour, best match first.
# (see https://redmine.gromacs.org/issues/1901 )
def find_matching_dihedral(dihtype, ai, aj, ak, al, dfun):
    origindex = [ai, aj, ak, al]
    for nmatch in [4,3,2,1,0]:
        wildcards = [i >= nmatch for i in range(4)]
        while True:
            for fwddir in [True, False]:
                if fwddir:
                    ixs = copy.deepcopy(origindex)
                else:
                    ixs = origindex[::-1]
                for i in range(4):
                    if wildcards[i]:
                        ixs[i] = "X"
                key = tuple(ixs + [dfun])
                if key in dihtype:
                    return dihtype[key]
            (ret, wildcards) = next_permutation(wildcards)
            if not ret:
                break
    raise RuntimeError("Could not find dihedral for %s-%s-%s-%d" % ai, aj, ak, al, dfun)

# Note griddata spans [-pi, pi)
def approximate_dih(griddata):
    """Approximate equispaced function by a cosine seris.
"""
    n = len(griddata)
    # Since we can't use cosine transformation library, do by ourselves.
    # E.g. n=24:
    # V(phi) = C0 + C1 sin(phi) + C1' cos(phi) + C2 sin(2 phi) + C2' cos(2 phi) + ... + C23' cos(23 phi).
    # This is achieved by:
    # Ck = 1/n sum_i exp (-k 2pi i j/n) V(2pi i/n)
    # We then throw out the high frequency ones and convert sin / cos with a phase.
    res = []
    for k in range(0, n):
        ck = 0. + 0.j
        for i in range(n):
            # V(2pi i / n) = griddata((i + n/2) % n)
            v = griddata[(i + n // 2) % n]
            ck += cmath.rect(1., - k * 2 * math.pi * i / n) * v
        ck /= n
        res.append(ck)
    # sanity check...
    for i in range(n):
        v = griddata[(i + n // 2) % n]
        pred = 0.
        for k in range(0, n):
            pred += res[k] * cmath.rect(1., k * 2. * math.pi * i / n)
        assert abs(v - pred) < 1e-8
    # Now we have complex factors. Then translate them into cos(n phi - phase) form.
    # To do this, we use: Re[c exp(k phi j)] = |c| exp(j (k phi + theta)) where c = |c| * exp(j theta)
    # The target function will thus be just -arg(c).
    cosseries = [(abs(c), -cmath.phase(c)) for c in res]
    # sanity check again...
    for i in range(n):
        v = griddata[(i + n // 2) % n]
        phi = 2 * math.pi * i / n
        pred = 0.
        for k in range(0, n):
            amp, phis = cosseries[k]
            pred += amp * math.cos(k * phi - phis)
        assert abs(v - pred) < 1e-8

    # Since this is DFT of real series, higher frequency cosines are just mirror of the function.
    # Cut the higher half.
    cosseries_half = [cosseries[i] for i in range(n // 2 + 1)]
    for i in range(1, n // 2):
        cosseries_half[i] = (cosseries_half[i][0] * 2, cosseries_half[i][1])

    # sanity check once again.
    for i in range(n):
        v = griddata[(i + n // 2) % n]
        phi = 2 * math.pi * i / n
        pred = 0.
        for (k, ap) in enumerate(cosseries_half):
            amp, phis = ap
            pred += amp * math.cos(k * phi - phis)
        assert abs(v - pred) < 1e-8

    # Finally remove high freqs.
    # TODO: parametrize this order
    order = 6
    cosseries_half = cosseries_half[0:(order+1)]
    
    return cosseries_half


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""Converts topology with REST2 protocol.
    Input topology must be preprocessed topology (generated by gmx grompp -pp). Also the atom types you want to scale with REST2 must be appended with "_". See PLUMED2's partial_tempering module for details.
    """)
    parser.add_argument('topology', action='store', type=str, 
                        help='Topology file (must be preprocessed and underlined)')
    parser.add_argument('output', action='store', type=str, 
                        help='Topology file output')
    parser.add_argument('--temp0', action='store', type=float, default=300.0, 
                        help='Simulation temperature')
    parser.add_argument('--temp', action='store', type=float, required=True,
                        help='Requesting temperature (lambda=T0/T)')
    
    parser.add_argument('--suffix', action='store', type=str, default="_", 
                        help='Suffix character')
    parser.add_argument('--ignore-noninteger-periodicity', dest='ignore_noninteger_periodicity', action='store_true',
                        help='Some dihedral parameters require periodicity sections, which should typically be integer. Instead of aborting on float, this option ignores non-integer input.')

    args = parser.parse_args()

    scale = args.temp0 / args.temp
    if scale > 1.0:
        sys.stderr.write("Warning: scaling factor exceeds 1.0 (temp > temp0)\n")
    sqrtscale = math.sqrt(scale)

    with open(args.topology) as fh, open(args.output, "w") as ofh:
        bondtype_of_atomtype = {}
        atomtype_info = {}
        dummy_atomtypes = set()
        dihtype = {}
        molecule = None
        sectiontype = None
        coulombrule = None
        residues = None
        atomnames = None
        cmaptypes_table = {}
        cmap_compensate_dihedrals = []
        def is_mainchain_nonpro(ai, aj, ak, al):
            ai -= 1
            aj -= 1
            ak -= 1
            al -= 1
            if (atomnames[ai], atomnames[aj], atomnames[ak], atomnames[al])\
               in [('O', 'C', 'N', 'H'), ('H', 'N', 'C', 'O')]: # AMBER nomenclature
                if atomnames[ai] == 'O' and residues[al] != 'PRO':
                    return True
                if atomnames[ai] == 'H' and residues[ai] != 'PRO':
                    return True
            if (atomnames[ai], atomnames[aj], atomnames[ak], atomnames[al])\
               in [('O', 'C', 'N', 'HN'), ('HN', 'N', 'C', 'O')]: # CHARMM nomenclature
                if atomnames[ai] == 'O' and residues[al] != 'PRO':
                    return True
                if atomnames[ai] == 'HN' and residues[ai] != 'PRO':
                    return True
            return False
        def print_cmap_compensation():
            if cmap_compensate_dihedrals != []:
                ofh.write("[ dihedrals ]\n")
                for c in cmap_compensate_dihedrals:
                    ofh.write("%s ; countering cmap\n" % c)
                ofh.write("\n")
                
        for lraw in fh:
            while lraw.endswith("\\\n"):
                lraw = "%s %s" % (lraw[:-2], next(fh)) # this is an undocumented feature in grompp, space is implicit
            ltmp = lraw.split(';', 1)
            if len(ltmp) == 1:
                l = ltmp[0]
                comment = ""
            else:
                l = ltmp[0]
                comment = ";" + ltmp[1]
            l = l.strip()
            ls = l.split()
            if l.startswith('#'):
                sys.stderr.write("The topology file is not preprocessed")
                sys.exit(1)
            if l.startswith('['):
                # Finish cmap terms and clear the buffer
                print_cmap_compensation()
                cmap_compensate_dihedrals = []
                
                sectiontype = ls[1]
                ofh.write(lraw)
                continue

            # blank line
            if len(ls) == 0:
                ofh.write(lraw)
                continue

            if sectiontype is None:
                pass
            elif sectiontype == 'defaults':
                combrule = int(ls[1])
            elif sectiontype == 'atomtypes':
                if len(ls) < 6:
                    raise RuntimeError("Atomtype contains < 6 fields")
                # here everything is mess but toppush.cpp is actually super mess 
                if len(ls[5]) == 1 and ls[5][0].isalpha():
                    have_bonded_type = True
                    have_atomic_number = True
                elif len(ls[3]) == 1 and ls[3][0].isalpha():
                    have_bonded_type = False
                    have_atomic_number = False
                else:
                    have_bonded_type = ls[1][0].isalpha()
                    have_atomic_number = not have_bonded_type

                atomtype = ls[0]
                (mass, charge, particle, sigc6, epsc12) = ls[1 + int(have_bonded_type) + int(have_atomic_number):]

                if have_bonded_type:
                    bondtype = ls[1]
                else:
                    bondtype = atomtype
                if have_atomic_number:
                    atomic_ix = 1 + int(have_bonded_type)
                    atomic_number = int(ls[atomic_ix])
                else:
                    atomic_number = 0 # ??
                    
                # store this because we use in [ atoms ] section
                bondtype_of_atomtype[atomtype] = bondtype

                mass = float(mass)
                charge = float(charge)
                sigc6 = float(sigc6)
                epsc12 = float(epsc12)
                is_dummy = epsc12 == 0.

                atomtype_info[atomtype] = (charge, mass)
                if is_dummy:
                    dummy_atomtypes.add(atomtype)

                # in dihedral-only gREST module it is not necessary to print scaled one
                # and the original line is also appended after this big if-elif-else block
            elif sectiontype == 'dihedraltypes':
                (ai, aj, ak, al) = ls[0:4]
                dihfun = int(ls[4])
                values = ls[5:]
                key = (ai, aj, ak, al, dihfun)
                if dihfun == 9:
                    # allows multiple dihedraltype for fn = 9
                    if key not in dihtype:
                        dihtype[key] = []
                    dihtype[key].append(values)
                else:
                    if key in dihtype:
                        for (i, e) in enumerate(dihtype[key]):
                            d = abs(float(values[i]) - float(e))
                            if d > 1e-20:
                                raise RuntimeError("Multiple dihedral for dihfun = %d, %s-%s-%s-%s"
                                                   % (dihfun, ai, aj, ak, al))
                    else:
                        dihtype[key] = values
                continue # suppress printing, we won't use dihedraltypes.
            elif sectiontype == 'cmaptypes':
                # CMAP types. See "sectiontype == 'cmap'" below.
                (ai, aj, ak, al, am) = ls[0:5]
                dihfun = int(ls[5])
                grid1 = int(ls[6])
                grid2 = int(ls[7])
                ngrid = grid1 * grid2 # current cmap should have this number
                assert len(ls[8:]) == ngrid
                themap = [[None for _ in range(grid2)] for _ in range(grid1)] # can be accessed with themap[<grid1][<grid2]
                ofh.write("%s" % (" ".join(ls[0:8])))
                for (i, v) in enumerate(ls[8:]):
                    v = float(v)
                    themap[i // grid2][i % grid2] = v
                    if i % grid2 == 0:
                        ofh.write("\\\n") # Hack: prevent 4095 chars line limit
                    else:
                        ofh.write(" ")
                    ofh.write("%.8f" % v)
                ofh.write("\n\n")
                # get grid average. I wish I could use numpy here, but to run this script on some closed environment...
                avg_grid1 = []
                for i in range(grid1):
                    s = 0.
                    for j in range(grid2):
                        s += themap[i][j]
                    avg_grid1.append(s / float(grid2))
                avg_grid2 = []
                for j in range(grid2):
                    s = 0.
                    for i in range(grid1):
                        s += themap[i][j]
                    avg_grid2.append(s / float(grid1))
                rb1 = approximate_dih(avg_grid1)
                rb2 = approximate_dih(avg_grid2)
                cmaptypes_table[(ai, aj, ak, al, am, dihfun)] = (rb1, rb2)
                continue
            elif sectiontype == 'moleculetype':
                molecule = ls[0]
                # These None are sentinels for 1-origin access
                bondtype_list = [None]
                scaled = [None]
                atomnames = []
                residues = []
            elif sectiontype == 'atoms':
                aindex = int(ls[0])
                atomtype = ls[1]
                # remove suffix to store atomname
                is_scaled = atomtype.endswith(args.suffix)
                if is_scaled:
                    canonical_atomtype = atomtype[:-len(args.suffix)]
                else:
                    canonical_atomtype = atomtype
                assert(aindex == len(scaled))
                scaled.append(is_scaled)
                bondtype_list.append(bondtype_of_atomtype[canonical_atomtype])

                # charge & mass is optional parameters, oof...
                if len(ls) > 6:
                    charge = float(ls[6])
                else:
                    (charge, _) = atomtype_info[canonical_atomtype]
                if len(ls) > 7:
                    mass = float(ls[7])
                else:
                    (_, mass) = atomtype_info[canonical_atomtype]
                if len(ls) > 8:
                    atomtypeB = ls[8]
                    if atomtypeB.endswith(args.suffix):
                        sys.stderr.write("Warning: typeB in atom line should not be suffixed (molecule %s, atom index %d)\n" % (molecule, aindex))
                    (chargeB, massB) = atomtype_info[atomtypeB]
                    fep = True
                    if is_scaled and not atomtypeB.endswith(args.suffix):
                        #atomtypeB += args.suffix
                        pass
                else:
                    fep = False
                    chargeB = 0.
                    massB = 0.
                    atomtypeB = None
                if len(ls) > 9:
                    chargeB = float(ls[9])
                if len(ls) > 10:
                    massB = float(ls[10])

                _resnr = ls[2]
                resid = ls[3]
                residues.append(resid)
                atomname = ls[4]
                atomnames.append(atomname)
                
                ofh.write("%5d %4s %4s %4s %4s %5s %16.8e %16.8e" % (aindex, canonical_atomtype, ls[2], ls[3], ls[4], ls[5], charge, mass))
                if fep:
                    ofh.write(" %4s %16.8e %16.8e%s\n" % (atomtypeB, chargeB, massB, comment.rstrip()))
                else:
                    ofh.write(" %s\n" % comment.rstrip())
                continue
            elif sectiontype == 'dihedrals':
                dihfun = int(ls[4])
                if dihfun in [1,2,3,4,5,9]:
                    (ai, aj, ak, al) = [int(x) for x in ls[0:4]]
                    sscale = 1.0
                    if scaled[ai]:
                        sscale *= sqrtscale
                    if scaled[al]:
                        sscale *= sqrtscale
                    if len(ls) == 5:
                        # must load dihedral table
                        (ti, tj, tk, tl) = [bondtype_list[x] for x in [ai, aj, ak, al]]
                        ofh.write("; parameters for %s-%s-%s-%s, fn=%d\n" % (ti, tj, tk, tl, dihfun))
                        params_tmp = find_matching_dihedral(dihtype, ti, tj, tk, tl, dihfun)
                        matched = True
                    else:
                        params_tmp = ls[5:]
                        matched = False
                    end_restrain = False
                    if dihfun == 9 and matched:
                        params_list = params_tmp
                    else:
                        params_list = [params_tmp]
                    if is_mainchain_nonpro(ai, aj, ak, al):
                        sys.stderr.write("stopped scaling %d-%d-%d-%d (%s:%s-%s:%s-%s:%s-%s:%s)\n" %
                                         (ai, aj, ak, al,
                                          residues[ai-1], atomnames[ai-1],
                                          residues[aj-1], atomnames[aj-1],
                                          residues[ak-1], atomnames[ak-1],
                                          residues[al-1], atomnames[al-1]))
                        sscale = 1.0

                    for params in params_list:
                        ofh.write("%5d %5d %5d %5d %2d" % (ai, aj, ak, al, dihfun))
                        # This table slightly eases the mess for dihfun / fep
                        (n_nonfep, to_scale, intind) = {
                            1: (3, [1], [2]),
                            2: (2, [1], []),
                            3: (6, [0, 1, 2, 3, 4, 5], []),
                            4: (3, [1], [2]),
                            5: (4, [0, 1, 2, 3], []),
                            9: (3, [1], [2])
                        }[dihfun]
                        if len(params) not in [n_nonfep, 2 * n_nonfep]:
                            raise RuntimeError("Number of args in dihedrals: expected %d or %d, but was %d (%d-%d-%d-%d:%d)" %
                                               (n_nonfep, 2 * n_nonfep, len(params),
                                                ai, aj, ak, al, dihfun))

                        nstates = 1
                        if sscale != 1.0:
                            nstates = 2
                        for j in range(nstates):
                            for i in range(n_nonfep):
                                v = params[i]
                                if i in intind:
                                    try:
                                        v = int(v)
                                    except ValueError:
                                        print(params)
                                        if args.ignore_noninteger_periodicity:
                                            vf = float(v)
                                            vi = int(vf)
                                            if abs(vi - vf) > 1e-2:
                                                raise RuntimeError("Periodicity should be integer but was %f" % vf)
                                            v = vi
                                        else:
                                            raise RuntimeError("Periodicity should be integer")
                                    fmts = "%1d" % v
                                else:
                                    v = float(v)
                                    if i in to_scale and j == 1:
                                        v *= sscale
                                    fmts = "%16.8e" % v
                                ofh.write(" %s" % fmts)
                        ofh.write("\n")
                    continue
            elif sectiontype == 'cmap':
                # CMAP! Dreadful CMAP! At least on GROMACS 2019, cmaptype cannot be handled with free-energy.
                # Here is where we do a hack: first calculate approximate functions such that
                # CMAP(phi, psi) ~= dihed1(phi) + dihed2(psi)
                # Then, for high tempreature replicas, modification potential of MODPOT = -c * dihed1 + -c * dihed2 
                # are added with c being between 0 to 1. 
                # with this way, "replica 0" will be composed of a correct cmap potential, while high temperature replica somewhat cancels the potential of CMAP.
                atomids = [int(x) for x in ls[0:5]]
                tys = [bondtype_list[ix] for ix in atomids]
                cmapfun = int(ls[5])
                assert cmapfun == 1
                apfun1, apfun2 = cmaptypes_table[(tys[0], tys[1], tys[2], tys[3], tys[4], cmapfun)]
                dihfun = 1 # proper dihedral
                def write_fun(idbeg, apfun):
                    sscale = 1.0
                    if scaled[atomids[idbeg]]:
                        sscale *= sqrtscale
                    if scaled[atomids[idbeg + 3]]:
                        sscale *= sqrtscale
                    for m, ap in enumerate(apfun):
                        if m == 0:
                            continue
                        coeff, phase = ap
                        phase_deg = phase * 360. / (2. * math.pi )
                        cmap_compensate_dihedrals.append("%s %d %f %f %d %f %f %d" % 
                                (" ".join([str(a) for a in atomids[idbeg:idbeg + 4]]),
                                 dihfun,
                                 phase_deg,
                                 0.,
                                 m,
                                 phase_deg,
                                 - coeff * sscale,
                                 m))
                write_fun(0, apfun1)
                write_fun(1, apfun2)

            # With a few exceptions, we just print as-is
            ofh.write(lraw)






