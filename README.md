# rest2py: REST2 type calculation on GROMACS, with or without PLUMED2 hrex module

This repository is dedicated to REST2-type replica exchange simulations. Documents are heavily under construction.

## Ingredients

### `rest2py.py`
This script generetes parameter-scaled topology file for REST2 type calculation. Requires PLUMED2 hrex module to run actual simulations. Basically it is same as `partial_tempering.sh` in PLUMED2 package, but with some additional features.

### `grestdih_nohrex.py`
This script generates parameter-scaled topology file for dihedral gREST by Kamiya et. al. Only dihedral angles are scaled. The generated structure does not scale non-bonded interaction and thus we do not need hrex-type calculation. Therefore, it can be run on unmodified GROMACS with single precision very well, and GPU works very efficiently there. 

### `replica_optimier.py`
This script is used to optimize replica exchange parameters. This is useful for FEP-type calculation where bonded / nonbonded parameters should be fine-tuned for a good exchange ratio.

### `canonicalize_top.py`
This script removes `[ bondtypes ]` `[ angletypes ]` and `[ dihedraltypes ]` section from the topology and converts corresponding `[ bonds ]` `[ angles ]` `[ dihedrals ]` section into a long format.

## Basic Usage

### `rest2py.py`

A typical usage to generate topology for `rest2py.py` is as follows:

```sh
touch dummy.mdp
gmx grompp -f dummy.mdp -p topol.top -c conf.gro -pp input.pp.top
vi input.pp.top     # here you add underlines to scaled atoms
python rest2py.py input.pp.top output600.top --temp 600 --temp0 300
python rest2py.py input.pp.top output500.top --temp 500 --temp0 300
python rest2py.py input.pp.top output300.top --temp 300 --temp0 300
```

First `gmx grompp` part is to generate "preprocessed" GROMACS topology file. This can be done with `gmx grompp -pp` ooption. This removes all `#include` sections as well as `#ifdef` and such. Then, you will specify the region you want to soften (scale the potential) by adding `"_"` to the atomtype. For example, if there is a topology file with `[ atoms ]` section:
```text
[ atoms ]
;   nr       type  resnr residue  atom   cgnr     charge       mass  typeB    chargeB      massB
; residue   1 SER rtp SER  q +1.0
     1        NH3      1    SER      N      1       -0.3     14.007   ; qtot -0.3
     2         HC      1    SER     H1      2       0.33      1.008   ; qtot 0.03
     3         HC      1    SER     H2      3       0.33      1.008   ; qtot 0.36
     4         HC      1    SER     H3      4       0.33      1.008   ; qtot 0.69
     5        CT1      1    SER     CA      5       0.21     12.011   ; qtot 0.9
...
```
Changing it to 
```text
[ atoms ]
;   nr       type  resnr residue  atom   cgnr     charge       mass  typeB    chargeB      massB
; residue   1 SER rtp SER  q +1.0
     1        NH3_      1    SER      N      1       -0.3     14.007   ; qtot -0.3
     2         HC_      1    SER     H1      2       0.33      1.008   ; qtot 0.03
     3         HC_      1    SER     H2      3       0.33      1.008   ; qtot 0.36
     4         HC_      1    SER     H3      4       0.33      1.008   ; qtot 0.69
     5        CT1      1    SER     CA      5       0.21     12.011   ; qtot 0.9
...
```
will scale first 4 atoms in the system. You don't have to modify other sections like `[ bondtypes ]` or such, just `[ atoms ]` section.

#### Important options

`--no-exclude-peptides`: `rest2py.py` typically *forbids* dihedral angle parameters around peptide omega angle to be scaled. This is to prevent cis peptide bond being formed during the REST2 calculation. If you dare to find such unusual structures, you should disable this safety protection by `--no-exclude-peptides`.

### `grestdih_nohrex.py`

Usage is almost same as `rest2py.py`. You need to preprocess and add underline to prepare input topology file.

```sh
python grestdih_nohrex.py --temp0 300.0 --temp 600.0 input.top output.top
```

To run the Hamiltonian replica exchange simulation, add following lines to mdp file:
```
free-energy = yes
fep-lambdas = 0 0.33 0.66 1.0 ; <- i/(N-1) suffices, where N is the number of replicas
init-lambda-state = 0         ; <- change this number according to the simulation state, 0 to (N-1)
calc-lambda-neighbors = 1     ; <- 1 is default. To use MBAR or such use -1
```

I am using the following snippet (in zsh) to generate mdp files + tpr files.

```zsh
LAMBDAS=()
for i in {0..$((NREP-1))}; do
  # equispaced.
  (( LAMBDA = i / (NREP - 1.0) )) || true
  LAMBDAS+=$LAMBDA
done
for i in {0..$((NREP-1))}; do
  wdir=$CODE/rest$i
  mkdir $wdir || true

  # Generate mdp file
  sed -e "s/%LAMBDAS%/$LAMBDAS/;s/%LAMBDASTATE%/$i/" mdp/run.mdp > $wdir/run.mdp
  # ^- In skeleton mdp file I write "fep-lambdas = %LAMBDAS%" and "init-lambda-state = %LAMBDASTATE%"

  # prep tpr file
  $GMX_SINGLE grompp -f $wdir/run.mdp -c $CODE/nptfin.gro -p $CODE/topol.fep.top -o $wdir/run -po $wdir/run.po.mdp -maxwarn 1 -r $CODE/conf_ionized.gro
done
```
